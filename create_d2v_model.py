import os
import json
import time
from stop_words import get_stop_words
from gensim.models.doc2vec import TaggedDocument,Doc2Vec
import psycopg2
from datetime import datetime

dbname=os.environ['PGCONNECT_DBNAME']
user=os.environ['PGCONNECT_USER']
password=os.environ['PGCONNECT_PASSWORD']
host= os.environ['PGCONNECT_HOST']
port=os.environ['PGCONNECT_PORT']

def create_doc2vec_model(articles,lang,size=100,epochs=10):
    # import stopwords for specific language of model
    stop_words = get_stop_words(lang)
    art_ids = []
    for art in articles:
        id_str = art[0]
        #strip opwords article docs
        nostop = [i for i in art[1].lower().split() if i not in stop_words]
        item = [id_str,nostop]
        art_ids.append(item)
    #strip stopwords article docs
    tagged = [TaggedDocument(doc,[i]) for i,doc in art_ids]
    # instantiate doc2vec model with parameters - size = # of nums representing each doc (100), min_count - occurences of words in vocab (filter out rare words), iter - passes to create vectors
    model = Doc2Vec(vector_size=size, min_count=2, epochs=10)
    ## build vocab from all tagged docs
    model.build_vocab(tagged)
    ## train model on tagged docs - total examples - total # of docs
    model.train(tagged,total_examples=model.corpus_count,epochs=epochs)
    # save model with language - eg esmodel.model for spanish docs
    model_name = lang + 'model3.model'
    model.save(model_name)
    model.delete_temporary_training_data(keep_doctags_vectors=True, keep_inference=True)

    print('saved as: ' + model_name)

def fetch_all_arts(lang):
    col_name = lang +'_arts'
    conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port, sslmode='require')
    cur = conn.cursor()
    sql = f"SELECT art_id,article FROM {col_name} WHERE dt > NOW() - interval '30 DAYS'"
    cur.execute(sql)
    articles  = cur.fetchall()
    conn.close()
    return articles


def create_d2v_model(lang):
    t2 = datetime.now()
    arts_all = fetch_all_arts(lang)
    print(lang + 'arts ' + str(len(arts_all))+  'loaded' + str(datetime.now() - t2))
    create_doc2vec_model(arts_all,lang,size=100,epochs=10)
    print('model: ' + str(datetime.now() - t2))

def main():
    langs = ['de','en','es','fr']
    for lang in langs:
        create_d2v_model(lang)

if __name__ == '__main__':
    main()
